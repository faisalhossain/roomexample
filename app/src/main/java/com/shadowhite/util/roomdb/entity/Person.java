package com.shadowhite.util.roomdb.entity;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class Person {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private String occupation;
    @Ignore
    private String note;

    public Person() {
    }

    public Person(int id, String name, String occupation, String note) {
        this.id = id;
        this.name = name;
        this.occupation = occupation;
        this.note = note;
    }

    public Person(String name, String occupation, String note) {
        this.name = name;
        this.occupation = occupation;
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", occupation='" + occupation + '\'' +
                ", note='" + note + '\'' +
                '}';
    }
}
