package com.shadowhite.util.roomdb.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.shadowhite.util.roomdb.dao.RoomDao;
import com.shadowhite.util.roomdb.entity.Person;

@Database(entities = {Person.class}, version = 1,exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract RoomDao roomDao();

}
