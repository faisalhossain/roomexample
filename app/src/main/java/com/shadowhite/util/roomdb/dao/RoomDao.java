package com.shadowhite.util.roomdb.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.shadowhite.util.roomdb.entity.Person;

import java.util.List;

@Dao
public interface RoomDao {
    @Query("SELECT * FROM Person")
    public List<Person> getPersonList();
    @Insert
    public void insertPersonData(Person person);
    @Update
    public void updatePerson(Person person);
    @Delete
    public void deletePerson(Person person);
}
