package com.shadowhite.roomdatabasetutorial.main;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.shadowhite.roomdatabasetutorial.R;
import com.shadowhite.util.roomdb.database.AppDatabase;
import com.shadowhite.util.roomdb.database.DatabaseClient;
import com.shadowhite.util.roomdb.entity.Person;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new DatabaseExecutor2().execute();
    }
    public class DatabaseExecutor2 extends AsyncTask<Void,Void,Void>{
    List list;
        @Override
        protected Void doInBackground(Void... voids) {
           list= DatabaseClient.getInstance(MainActivity.this).getAppDatabase().roomDao().getPersonList();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (list!=null)
                    {
                        Log.d("chk",list.toString());
                    }
                    else {
                        Log.d("chk","list is null");
                    }
                }
            });
        }
    }
    public class DatabaseExecutor extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            DatabaseClient.getInstance(MainActivity.this).getAppDatabase().roomDao().insertPersonData(new Person("Name","student","some note"));
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainActivity.this,"Success",Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}
